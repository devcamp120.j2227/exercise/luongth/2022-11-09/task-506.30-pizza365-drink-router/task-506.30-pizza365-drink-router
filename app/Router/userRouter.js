

// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
//const drinkMiddleware = require("../middleware/drinkmiddleware");

router.get("/users", (request, response) => {
    response.json({
        message: "Get ALL Users"
    })
})
module.exports = router