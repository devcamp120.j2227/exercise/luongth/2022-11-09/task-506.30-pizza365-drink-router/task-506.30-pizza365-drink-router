// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const drinkSchema = new Schema({
   
    maNuocUong: {
        type: String,
        required: true,
        unique: true
    },
   
    tenNuocUong: {
        type:String,
        required: true,
    },
     donGia: {
        type: Number,
        required: true
    },
    // // Một course có nhiều review
    // reviews: [{
    //     type: mongoose.Types.ObjectId,
    //     ref: "Review"
    // }]
    // Trường hợp 1 course có 1 reivew 
    // reviews: {
    //     type: mongoose.Types.ObjectId,
    //     ref: "Review"
    // }
}, {
    timestamps: true
})

module.exports = mongoose.model("Drink", drinkSchema);